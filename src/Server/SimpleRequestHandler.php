<?php

namespace Server;

interface SimpleRequestHandler
{
    public function processRequest($request);
}
