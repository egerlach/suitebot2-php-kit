<?php

namespace Ai;

class SampleBotAi implements BotAi
{
    private $myId = null;
    private $gamePlan = null;
    private $players = null;
    private $myPosition = null;
    private $round = 0;
    private $enemyFired = false;

    /**
     * This method is called at the beginning of the game.
     *
     * @param GameSetup $gameSetup details of the game, such as the game plan dimensions, the list of players, etc
     *
     * @return the first move to play
     */
    public function initializeAndMakeMove($gameSetup)
    {
        $this->myId = $gameSetup->aiPlayerId;
        $this->gamePlan = $gameSetup->gamePlan;
        $this->players = $gameSetup->players;
        $this->round = 1;

        $this->setMyStartingPosition();

        return $this->figureOutMove();
    }

    /**
     * This method is called in each round, except for the first one.
     *
     * @param $gameRound all players' moves in the previous round
     *
     * @return the move to play
     */
    public function makeMove($gameRound)
    {
        ++$this->round;

        $this->enemyFired = false;
        foreach ($gameRound->moves as $playerMove) {
            // update the state of our game with all the other players' moves from the previous round
            $this->simulateMove($playerMove);
        }

        // return our move for the next round
        return $this->figureOutMove();
    }

    private function setMyStartingPosition()
    {
        $player = new \Game\Player($this->myId, null);
        $myIndex = array_search($player, $this->players);
        $this->myPosition = $this->gamePlan->startingPositions[$myIndex];
    }

    private function simulateMove($playerMove)
    {
        if ($playerMove->playerId != $this->myId && $playerMove->move == 'FIRE') {
            $this->enemyFired = true;
        }
    }

    private function figureOutMove()
    {
        if ($this->enemyFired) {
            // If they have fired, we will fire too!
            return 'FIRE';
        } else {
            if ($this->round < $this->gamePlan->maxRounds / 2) {
                return 'WARM UP';
            } else {
                return 'GO LEFT';
            }
        }
    }
}
