<?php

namespace Ai;

interface BotAi
{
    /**
     * This method is called at the beginning of the game.
     *
     * @param gameSetup details of the game, such as the game plan dimensions, the list of players, etc
     *
     * @return the first move to play
     */
    public function initializeAndMakeMove($gameSetup);

    /**
     * This method is called in each round, except for the first one.
     *
     * @param gameRound all players' moves in the previous round
     *
     * @return the move to play
     */
    public function makeMove($gameRound);
}
