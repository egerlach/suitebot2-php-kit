<?php

namespace Game;

class GamePlan
{
    public $width = 0;
    public $height = 0;
    public $maxRounds = 0;
    public $startingPositions = array();

    /**
     * GamePlan constructor.
     *
     * @param array $props an array containing width, height, startingPositions and maxRounds
     *                     e.g.
     *                     {
     *                     "width":10,
     *                     "height":20,
     *                     "startingPositions":[
     *                     {"x":2,"y":2},
     *                     {"x":7,"y":7}
     *                     ],
     *                     "maxRounds":25
     *                     }
     */
    public function __construct($props)
    {
        $this->width = $props['width'];
        $this->height = $props['height'];
        foreach ($props['startingPositions'] as $point) {
            $this->startingPositions[] = new Point($point);
        }
        $this->maxRounds = $props['maxRounds'];
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'width' => $this->width,
                'height' => $this->height,
                'startingPositions' => $this->startingPositions,
                'maxRounds' => $this->maxRounds,
            )
        );

        return 'Gameplan'.$json;
    }
}
