<?php

namespace Game;

class Point
{
    public $x = null;
    public $y = null;

    /**
     * Point constructor.
     *
     * @param array $props array containing x and y
     * @param $x
     * @param $y
     */
    public function __construct($props)
    {
        $this->x = $props['x'];
        $this->y = $props['y'];
    }

    /**
     * Two points are considered equal if they occupy the same x and y coordinates.
     *
     * @param Point $point
     *
     * @return bool
     */
    public function equals($point)
    {
        if ($this === $point) {
            return true;
        }
        if ($point == null) {
            return false;
        }
        if (!($point instanceof Point)) {
            return false;
        }

        return $point->x == $this->x && $point->y == $this->y;
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'x' => $this->x,
                'y' => $this->y,
            )
        );

        return 'Point'.$json;
    }
}
