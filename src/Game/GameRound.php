<?php

namespace Game;

class GameRound
{
    // a collection of PlayerMoves
    public $moves = array();

    /**
     * GameRound constructor.
     *
     * @param $props array containing moves
     *
     * e.g.
     * {
     * 		"moves":[
     * 			{"playerId":1,"move":"FIRE"},
     *			{"playerId":2,"move":"JUMP"}
     *		]
     *	}
     */
    public function __construct($props)
    {
        foreach ($props['moves'] as $move) {
            $this->moves[] = new PlayerMove($move);
        }
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'moves' => $this->moves,
            )
        );

        return 'GameRound'.$json;
    }
}
