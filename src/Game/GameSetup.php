<?php

namespace Game;

class GameSetup
{
    public $aiPlayerId = null;
    public $players = array();
    public $gamePlan = null;

    /**
     * GameSetup constructor.
     *
     * @param array $props an array, containing aiPlayerId, $players, and $gamePlan
     *
     * e.g.
     * {
     * 		"aiPlayerId":1,
     *		"players":[
     *			{
     *				"id":2,
     *				"name":"R2D2"
     *			},
     *			{
     *				"id":1,
     *				"name":"C-3PO"
     *			}
     *		],
     *		"gamePlan":{
     *			"width":10,
     *			"height":20,
     *			"startingPositions":[
     *				{"x":2,"y":2},
     *				{"x":7,"y":7}
     *			],
     *			"maxRounds":25
     *		}
     * }
     */
    public function __construct($props)
    {
        $this->aiPlayerId = $props['aiPlayerId'];
        foreach ($props['players'] as $player) {
            $this->players[] = new Player($player);
        }
        $this->gamePlan = new GamePlan($props['gamePlan']);
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'aiPlayerId' => $this->aiPlayerId,
                'players' => $this->players,
                'gamePlan' => $this->gamePlan,
            )
        );

        return 'GameSetup'.$json;
    }
}
