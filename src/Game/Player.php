<?php

namespace Game;

class Player
{
    public $id = null;
    public $name = '';

    /**
     * Player constructor.
     *
     * @param array $props an array containing id and name
     */
    public function __construct($props)
    {
        $this->id = $props['id'];
        $this->name = $props['name'];
    }

    /**
     * @param $o
     *
     * @return bool true, if passed itself?
     */
    public function equals($o)
    {
        if ($this === $o) {
            return true;
        }
        if ($o == null) {
            return false;
        }
        if (get_class($o) != 'Player') {
            return false;
        }

        return $o->id == $this->id;
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'id' => $this->id,
                'name' => $this->name,
            )
        );

        return 'Player'.$json;
    }
}
