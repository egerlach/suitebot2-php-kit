<?php

namespace Game;

class PlayerMove
{
    public $playerId = null;
    public $move = '';

    /**
     * PlayerMove constructor.
     *
     * @param array props array containing playerId and move
     *
     * {"playerId":1,"move":"FIRE"}
     */
    public function __construct($props)
    {
        $this->playerId = $props['playerId'];
        $this->move = $props['move'];
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'playerId' => $this->playerId,
                'move' => $this->move,
            )
        );

        return 'PlayerMove'.$json;
    }
}
