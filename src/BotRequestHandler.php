<?php

class BotRequestHandler implements Server\SimpleRequestHandler
{
    public function __construct($botAi)
    {
        $this->botAi = $botAi;
    }

    public function processRequest($request)
    {
        $messageType = $this->decodeMessageType($request);

        switch ($messageType) {
        case 'SETUP':
            return $this->botAi->initializeAndMakeMove($this->decodeSetupMessage($request));
        case 'MOVES':
            return $this->botAi->makeMove($this->decodeMovesMessage($request));
        default:
            return 'UNEXPECTED REQUEST: '.$request;
        }
    }

    public function decodeMessageType($request)
    {
        $json = json_decode($request, true);

        return strtoupper($json['messageType']);
    }

/*
 A setup request will look like this:

{
    "messageType":"setup",
    "aiPlayerId":1,
    "players":[
        {
            "id":2,
            "name":"R2D2"
        },
        {
            "id":1,
            "name":"C-3PO"
        }
    ],
    "gamePlan":{
        "width":10,
        "height":20,
        "startingPositions":[
            {"x":2,"y":2},
            {"x":7,"y":7}
        ],
        "maxRounds":25
    }
}

A moves request will look like this:

{
    "messageType":"moves",
    "moves":[
        {"playerId":1,"move":"FIRE"},
        {"playerId":2,"move":"JUMP"}
    ]
}

 */

    /**
     * @param $request
     *
     * @return a GameSetup object
     */
    public function decodeSetupMessage($request)
    {
        $json = json_decode($request, true);

        return new Game\GameSetup($json);
    }

    /**
     * @param $request
     *
     * @return a GameRound object
     */
    public function decodeMovesMessage($request)
    {
        $json = json_decode($request, true);

        return new Game\GameRound($json);
    }
}
