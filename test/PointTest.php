<?php

use PHPUnit\Framework\TestCase;
use Game\Point;

class PointTest extends TestCase
{
    /**
     * @dataProvider equalsProvider
     */
    public function testEquals($point1, $point2, $expected)
    {
        $point1 = new Point($point1);
        $point2 = new Point($point2);
        $isEqual = $point1->equals($point2);
        $this->assertEquals($isEqual, $expected);
    }

    public function equalsProvider()
    {
        return array(
            array(
                'point1' => array('x' => 0, 'y' => 0),
                'point2' => array('x' => 0, 'y' => 0),
                'expected' => true,
            ),
            array(
                'point1' => array('x' => 13, 'y' => 12),
                'point2' => array('x' => 13, 'y' => 12),
                'expected' => true,
            ),
            array(
                'point1' => array('x' => 0, 'y' => 12),
                'point2' => array('x' => 1, 'y' => 12),
                'expected' => false,
            ),
            array(
                'point1' => array('x' => -5, 'y' => -5),
                'point2' => array('x' => -5, 'y' => -2),
                'expected' => false,
            ),
        );
    }

    /**
     * @dataProvider stringProvider
     */
    public function testToString($point, $expected)
    {
        $point = new Point($point);
        $string = (string) $point;
        $this->assertEquals($string, $expected);
    }

    public function stringProvider()
    {
        return array(
            array(
                'point' => array('x' => 0, 'y' => 0),
                'expected' => 'Point{"x":0,"y":0}',
            ),
            array(
                'point' => array('x' => 13, 'y' => 12),
                'expected' => 'Point{"x":13,"y":12}',
            ),
            array(
                'point' => array('x' => 0, 'y' => 12),
                'expected' => 'Point{"x":0,"y":12}',
            ),
            array(
                'point' => array('x' => -5, 'y' => -5),
                'expected' => 'Point{"x":-5,"y":-5}',
            ),
        );
    }
}
