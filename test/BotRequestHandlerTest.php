<?php

use PHPUnit\Framework\TestCase;
use Ai\BotAi;
use Game\Player;
use Game\GamePlan;
use Game\Point;

class BotRequestHandlerTest extends TestCase
{
    const SETUP_MESSAGE =
            '{'.
                    '"messageType":"setup",'.
                    '"aiPlayerId":1,'.
                    '"players":[{"id":2,"name":"R2D2"},{"id":1,"name":"C-3PO"}],'.
                    '"gamePlan":{'.
                        '"width":10,'.
                        '"height":20,'.
                        '"startingPositions":[{"x":2,"y":2},{"x":7,"y":7}],'.
                        '"maxRounds":25'.
                    '}'.
            '}';

    const MOVES_MESSAGE =
            '{'.
                    '"messageType":"moves",'.
                    '"moves":[{"playerId":1,"move":"FIRE"},{"playerId":2,"move":"JUMP"}]'.
            '}';
    public function setUp()
    {
        $this->dummyBotAi = new DummyBotAi();
        $this->requestHandler = new BotRequestHandler($this->dummyBotAi);
    }

    public function testOnSetUpMessageInitializeShouldBeCalled()
    {
        $aiMove = $this->requestHandler->processRequest(self::SETUP_MESSAGE);
        $this->assertFalse($this->dummyBotAi->makeMoveCalled);
        $this->assertEquals('FIRSTMOVE', $aiMove);
    }

    public function testObjectCreate()
    {
        $botRequestHandler = new BotRequestHandler(new Ai\SampleBotAi());
        $object = $botRequestHandler->decodeSetupMessage(self::SETUP_MESSAGE);

        $this->assertObjectNotHasAttribute('messageType', $object);
        $this->assertObjectHasAttribute('aiPlayerId', $object);
        $this->assertObjectHasAttribute('players', $object);
        $this->assertObjectHasAttribute('gamePlan', $object);

        $this->assertEquals(1, $object->aiPlayerId);

        $this->assertInstanceOf(Player::class, $object->players[0]);
        $this->assertInstanceOf(Player::class, $object->players[1]);

        $this->assertEquals(2, $object->players[0]->id);
        $this->assertEquals('R2D2', $object->players[0]->name);
        $this->assertEquals(1, $object->players[1]->id);
        $this->assertEquals('C-3PO', $object->players[1]->name);

        $this->assertInstanceOf(GamePlan::class, $object->gamePlan);
        $this->assertEquals(10, $object->gamePlan->width);
        $this->assertEquals(20, $object->gamePlan->height);
        $this->assertEquals(25, $object->gamePlan->maxRounds);

        $this->assertCount(2, $object->gamePlan->startingPositions);
        $this->assertInstanceOf(Point::class, $object->gamePlan->startingPositions[0]);
        $this->assertInstanceOf(Point::class, $object->gamePlan->startingPositions[1]);

        $this->assertEquals(2, $object->gamePlan->startingPositions[0]->x);
        $this->assertEquals(2, $object->gamePlan->startingPositions[0]->y);
        $this->assertEquals(7, $object->gamePlan->startingPositions[1]->x);
        $this->assertEquals(7, $object->gamePlan->startingPositions[1]->y);
    }
}

class DummyBotAi implements BotAi
{
    public $initializeAndMakeMoveCalled = false;
    public $makeMoveCalled = false;

    public function initializeAndMakeMove($gameSetup)
    {
        $initializeAndMakeMoveCalled = true;

        return 'FIRSTMOVE';
    }

    public function makeMove($gameRound)
    {
        $makeMoveCalled = true;

        return 'NEXTMOVE';
    }
}
