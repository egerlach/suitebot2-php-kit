<?php

use PHPUnit\Framework\TestCase;
use Game\PlayerMove;

class MovesTest extends TestCase
{
    public function testObjectCreate()
    {
        $movesJson = '
			{
				"messageType":"moves",
				"moves":[
					{"playerId":1,"move":"FIRE"},
					{"playerId":2,"move":"JUMP"}
				]
			}
		';

        $botRequestHandler = new BotRequestHandler(new Ai\SampleBotAi());
        $object = $botRequestHandler->decodeMovesMessage($movesJson);

        $this->assertObjectNotHasAttribute('messageType', $object);
        $this->assertObjectHasAttribute('moves', $object);

        $this->assertCount(2, $object->moves);
        $this->assertInstanceOf(PlayerMove::class, $object->moves[0]);
        $this->assertInstanceOf(PlayerMove::class, $object->moves[1]);

        $this->assertEquals(1, $object->moves[0]->playerId);
        $this->assertEquals('FIRE', $object->moves[0]->move);
        $this->assertEquals(2, $object->moves[1]->playerId);
        $this->assertEquals('JUMP', $object->moves[1]->move);
    }
}
