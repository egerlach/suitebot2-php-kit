<?php

echo "\n\n\n\n\n";

$address = '127.0.0.1';
$port = 9001;

send_to_server('FOO');
sleep(1);

send_to_server('UPTIME');
sleep(1);

send_to_server('UPTIME');
sleep(1);

send_to_server('UPTIME');
sleep(1);

$setupJson = json_encode(json_decode('
	{
		"messageType":"setup",
		"aiPlayerId":1,
		"players":[
			{
				"id":2,
				"name":"R2D2"
			},
			{
				"id":1,
				"name":"C-3PO"
			}
		],
		"gamePlan":{
			"width":10,
			"height":20,
			"startingPositions":[
				{"x":2,"y":2},
				{"x":7,"y":7}
			],
			"maxRounds":10
		}
	}
'));

// this is round 1, the setup round
send_to_server($setupJson);
sleep(1);

// round 2
send_to_server('{"messageType":"moves","moves":[{"playerId":1,"move":"FIRE"},{"playerId":2,"move":"JUMP"}]}');
sleep(1);

// round 3
send_to_server('{"messageType":"moves","moves":[{"playerId":1,"move":"GO LEFT"},{"playerId":2,"move":"GO BACK"}]}');
sleep(1);

// round 4
send_to_server('{"messageType":"moves","moves":[{"playerId":1,"move":"FIRE"},{"playerId":2,"move":"FIRE"}]}');
sleep(1);

// round 5
send_to_server('{"messageType":"moves","moves":[{"playerId":1,"move":"FIRE"},{"playerId":2,"move":"JUMP"}]}');
sleep(1);

send_to_server('FOO');
sleep(1);

send_to_server('UPTIME');
sleep(1);

// round 6
send_to_server('{"messageType":"moves","moves":[{"playerId":1,"move":"FIRE"},{"playerId":2,"move":"GO RIGHT"}]}');
sleep(1);

// round 7
send_to_server('{"messageType":"moves","moves":[{"playerId":1,"move":"FIRE"},{"playerId":2,"move":"GO LEFT"}]}');
sleep(1);

// round 8
send_to_server('{"messageType":"moves","moves":[{"playerId":1,"move":"FIRE"},{"playerId":2,"move":"GO RIGHT"}]}');
sleep(1);

// round 9
send_to_server('{"messageType":"moves","moves":[{"playerId":1,"move":"FIRE"},{"playerId":2,"move":"FIRE"}]}');
sleep(1);

// round 10
send_to_server('{"messageType":"moves","moves":[{"playerId":1,"move":"FIRE"},{"playerId":2,"move":"GO RIGHT"}]}');
sleep(1);

// exit
send_to_server('EXIT');
sleep(1);

function send_to_server($text)
{
    global $address, $port;

    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

    echo "Creating socket...\n";
    if ($socket === false) {
        echo 'socket_create() failed: reason: '.socket_strerror(socket_last_error())."\n";
    } else {
        echo "socket created OK. \n";
    }

    echo "Attempting to connect to '$address' on port '$port'... \n";
    try {
        $result = socket_connect($socket, $address, $port);
    } catch (Exception $e) {
        echo "Connection failed.\n";
        die();
    }

    if ($result === false) {
        echo "socket_connect() failed.\n";
        echo 'Reason:'.socket_strerror(socket_last_error($socket))."\n";
        die();
    } else {
        echo "connected OK.\n\n";
    }

    $text = $text."\n";
    echo "\n".str_repeat('-', 100)."\n";
    echo 'Sending message to server: '.$text."\n";
    socket_write($socket, $text, strlen($text));
    while ($out = socket_read($socket, 2048)) {
        if ($out === '') {
            return;
        }
        echo 'Response from server: '.$out."\n";
        break;
    }
    echo 'Closing socket... ';
    socket_close($socket);
    echo "OK.\n\n";

    return;
}
